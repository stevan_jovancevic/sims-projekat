from PySide2.QtWidgets import QMenuBar, QMenu

class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit",self)
        self.help_menu = QMenu("Help", self)

        self._populate_menues()

    def _populate_menues(self):
        #actions = self.parent().get_actions()
        #self.help_menu.addAction(actions["help"])
        #self.help_menu.addAction(actions["about"])

        self.addMenu(self.file_menu)
        self.addMenu(self.edit_menu)
        self.addMenu(self.help_menu)

        
