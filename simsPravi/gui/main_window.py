from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.central_widget import CentralWidget
from gui.widgets.menu_bar import MenuBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.structure_dock_widget import StructureDockWidget


class MainWindow(QMainWindow):
    def __init__(self, parent: None, user: None):
        super().__init__(parent)

        self.setWindowTitle("RUKOVALAC INFORMACIONIM RESURSIMA")
        self.setWindowIcon(QIcon(""))
        self.resize(1000,800)
# INICIJALIZACIJA
        self.menu_bar = MenuBar(self)
        self.status_bar = StatusBar(self)
        self.tool_bar = ToolBar(self)
        self.central_widget = CentralWidget(self)
        self.file_dock_widget = StructureDockWidget("Struktura radnog prostora", self)

# UVEZIVANJE ELEMENATA GUI-JA
        self.addToolBar(self.tool_bar)
        self.setMenuBar(self.menu_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)

        self.status_bar.addWidget(QLabel("Ulogovani korisnik je: Sara Tovilovic"))

    def get_actions(self):
        return self.tool_bar.actions_dict

    def add_actions(self, where="toolbar", name="None", actions=[]):
        if where == "menu":
            ... # dodati u meni sa nazivom name? * u file meni, edit meni itd///
        elif where=="toolbar":    
            self.tool_bar.addSeparator()
            self.tool_bar.addActions(actions)
        

