from PySide2.QtWidgets import QDockWidget, QTreeView, QFileSystemModel
from PySide2.QtCore import QDir

class StructureDockWidget(QDockWidget):
    def __init__(self, title="Structure", parent=None):
        super().__init__(title, parent)
        self.structure_widget = QTreeView()
        self.structure_model = QFileSystemModel()
        self.structure_model.setRootPath(QDir.currentPath())
        self.structure_widget.setModel(self.structure_model)

        self.setWidget(self.structure_widget)
