from PySide2.QtWidgets import QDialog, QVBoxLayout, QLabel, QDialogButtonBox
from PySide2.QtGui import QIcon

class HelpDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Help")
        self.resize(300,200)
        self.setWindowIcon(QIcon("gui/resources/help"))

        self.layout = QVBoxLayout()

        self.help_label = QLabel("Korisnicka Pomoc: vise detalja na linku: ", self)
        self.buttons = QDialogButtonBox(QDialogButtonBox.Ok)
        
        self.buttons.accepted.connect(self.accept)
        #self.buttons.rejected.connect(self.reject)

        self.layout.addWidget(self.help_label)
        self.layout.addWidget(self.buttons)

        self.setLayout(self.layout)