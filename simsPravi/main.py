import sys
from PySide2.QtWidgets import QApplication
from gui.main_window import MainWindow

if __name__ == "__main__":
    app = QApplication([])
    window = MainWindow(parent=None, user="Sara Tovilovic")
    window.resize(1000,800)
    window.show()
    sys.exit(app.exec_())