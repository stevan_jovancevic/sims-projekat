from PySide2.QtWidgets import QToolBar, QAction, QMessageBox
from PySide2.QtGui import QIcon
from gui.widgets.help_dialog.help_dialog import HelpDialog

class ToolBar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        self.actions_dict = {}
        self.default_actions()
        self.bind_actions()


    def default_actions(self):
        exit_action = QAction(QIcon("gui/resources/exit"), "&Exit")

        help_action = QAction(QIcon("gui/resources/help"), "&Help")

        about_action = QAction(QIcon("gui/resources/about"), "&About")

        self.actions_dict["exit"] = exit_action
        self.actions_dict["help"] = help_action
        self.actions_dict["about"] = about_action
        about_action.setStatusTip("About program")

        self.addAction(exit_action)
        self.addAction(help_action)
        self.addAction(about_action)

    def bind_actions(self):
        # koja ce se metoda izvrsiti kada se trigerije data akcija
        self.actions_dict["about"].triggered.connect(self.on_about_action) # u connect se prosledjuje callback funkcije
        self.actions_dict["help"].triggered.connect(self.on_help_action)

    def on_about_action(self):
        QMessageBox.information(self.parent(), "About program", "Informacije o raznom", QMessageBox.Ok, QMessageBox.Close)

    def on_help_action(self):
        dialog = HelpDialog(self.parent())
        dialog.exec_()