from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView, QAction, QFileDialog, QAbstractItemView
from PySide2.QtGui import QIcon
from gui.widgets.data_view.data_model import DataModel
import csv
from gui.widgets.edit_dialog.edit_dialog import EditDialog

class DataView(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.widget_layout = QVBoxLayout()

        self.on_open_file_action = QAction(QIcon("gui/resources/open"), "Open")
        self.on_open_file_action.triggered.connect(self.on_open_file)

        self.data_view = QTableView()
        self.data_view.setSelectionMode(QAbstractItemView.SingleSelection)
        self.data_view.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.data_view.doubleClicked.connect(self.open_edit_form)

        self.widget_layout.addWidget(self.data_view)
        self.setLayout(self.widget_layout)
    """
    def generate_dummy_data(self):
        data = [
            ["270", "Marko", "Markovic"],
            ["271", "Petar", "Petrovic"],
            ["272", "Janko", "Jankovic"],
            ["273", "Lepa", "Petrovic"],
            ["274", "Jelena", "Kostic"],
        ]

        return data
    """
    def export_actions(self):
        return [self.on_open_file_action]

    def on_open_file(self):
        #izbor datoteke iz fajl sistema
        file_name = QFileDialog.getOpenFileName(self, "Open Data file", "gui/resources/data","CSV files (*.csv)")
        print(file_name)
        #kada se otvori dijalog a ne izabere datoteka
        if file_name[0] == "":
            return
        with open(file_name[0], "r", encoding="utf-8") as fp:
            reader = csv.reader(fp, delimiter=";")
            reader = list(reader)
            header = reader[0] # FIXME: ovo ce biti u metapodacima
            data = reader[1:] # od prve linije pa do kraja datoteke
            # kreiranje modela na osnovu ucitanih podataka
            self.data_model = DataModel(data=data, header_data=header)
            # postavljanje modela u prikaz
            self.data_view.setModel(self.data_model)
    
    def open_edit_form(self, index=None):
        model = self.data_view.model()
        selected_labels = model.get_headers()
        selected_data = model.get_row(index.row())
        # kreiranje dijaloga
        edit_dialog = EditDialog(self.parent())
        # populisanje dijaloga podacima iz reda tabela
        edit_dialog.enter_data(selected_labels, selected_data)
        #prikazivanje dijaloga
        result = edit_dialog.exec_()
        if result == 1:
            data = edit_dialog.get_data()
            model.replace_data(index.row(), data)


    