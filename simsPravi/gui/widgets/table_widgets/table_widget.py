from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableWidget, QTableWidgetItem

class TableWidget(QWidget):
    # kompozicija - pravljenje slozenog widgeta, na osnovu jednostavnih
    def __init__(self, parent=None):
        super().__init__(parent)

        self.widget_layout = QVBoxLayout()

        self.table_widget = QTableWidget(parent)
        self.table_widget.setColumnCount(12)
         # indeks, ime, prezime
        self.table_widget.setRowCount(10)
        self.table_widget.setHorizontalHeaderLabels(["Indeks", "Ime", "Prezime"])

       # self.fill_data()

        self.widget_layout.addWidget(self.table_widget)
        self.setLayout(self.widget_layout)

"""
 def fill_data(self):
        for i in range(10):
            kolona1 = QTableWidgetItem("2020/270" + str(i))
            kolona2 = QTableWidgetItem("Marko" + str(i))
            kolona3 = QTableWidgetItem("Markovic" + str(i))

            self.table_widget.setItem(i, 0, kolona1)
            self.table_widget.setItem(i, 1, kolona2)
            self.table_widget.setItem(i, 2, kolona3)

"""
   