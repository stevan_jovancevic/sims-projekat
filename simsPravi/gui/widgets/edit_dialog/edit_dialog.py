from PySide2.QtWidgets import QDialog, QVBoxLayout, QFormLayout, QDialogButtonBox, QLineEdit
from PySide2.QtGui import QIcon


class EditDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Edit data")
        self.setWindowIcon(QIcon("gui/resources/exit"))
        self.resize(400, 300)

        self.dialog_layout = QVBoxLayout()
        self.form_layout = QFormLayout()

        # dugmici
        self.button_box = QDialogButtonBox(QDialogButtonBox.Save)

        self.button_box.accepted.connect(self.accept)
       # self.button_box.rejected.connect(self.reject)

        # populisanje layout-a
        self.dialog_layout.addLayout(self.form_layout)
        self.dialog_layout.addWidget(self.button_box)
        
        self.setLayout(self.dialog_layout)

    def enter_data(self, labels=[], data=[]):
        # metoda pravi redove u formi tako da su labele iz liste labela
        # a na odgovarajucim pozicijama ce se dobaviti i podaci iz liste data
        for i in range(len(labels)):
            # FIXME: ako podatak ne postoji u celiji bice greska
            self.form_layout.addRow(labels[i], QLineEdit(data[i]))

    def get_data(self):
        # metoda koja iscitava podatke iz forme i vraca kao rezultat
        data = []
        for row_number in range(1, self.form_layout.rowCount()*2, 2):
            # prikupljanje teksta iz lineedit-a
            data.append(self.form_layout.itemAt(row_number).widget().text())
        return data
