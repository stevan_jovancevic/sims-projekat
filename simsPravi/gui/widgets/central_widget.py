from PySide2.QtWidgets import QWidget, QVBoxLayout
#from gui.widgets.table_widgets.table_widget import TableWidget
from gui.widgets.data_view.data_view import DataView

class CentralWidget(QWidget):
    def __init__(self, parent: None):
        super().__init__(parent)

        self.lejaut = QVBoxLayout(self)
        self.tabela = DataView()

        #main window
        self.parent().add_actions(actions=self.tabela.export_actions(), where="toolbar")

        self.lejaut.addWidget(self.tabela)
        
        #self.text = QTextEdit()
        #self.lejaut.addWidget(self.text)
        self.setLayout(self.lejaut)

