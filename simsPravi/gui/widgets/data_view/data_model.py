from PySide2.QtCore import QAbstractTableModel, QModelIndex, Qt

class DataModel(QAbstractTableModel):
    def __init__(self, parent=None, header_data=None, data=None):
        super().__init__(parent)
        self.table_data_list = data # lista listi, kao matrica
        self.header_data = header_data # moze da dodje uz data, u smislu, da bude prvi red

    def get_element(self, index:QModelIndex):
        if index.isValid():
            # element iz matrice dobijamo spram reda (matrica se indeksira) pa onda kolone indeksa datog
            element = self.table_data_list[index.row()][index.column()]
            if element:
                return element
        return self.table_data_list # vraca se glavna lista

    def get_row(self, row=0):
        if self.table_data_list is not None:
            if row < len(self.table_data_list):
                data_row = self.table_data_list[row]
                if data_row:
                    return data_row

    def get_headers(self):
        return self.header_data
    
    def replace_data(self, row, data = []):
        # emitovati signal za promenu, izvrsiti promenu, emitovati signal za kraj promene
        self.table_data_list[row] = data

    #redefinisano
    def rowCount(self, parent=...):
        return len(self.table_data_list) # koliko ima elemenata, podlisti, u listi
    
    def columnCount(self, parent=...):
        return 3 # svaki red ima tri podatka
    
    def data(self, index, role=...):
        element = self.get_element(index)
        if role == Qt.DisplayRole: # qt.decorationrole - za ikonice
            return element 
        
    def headerData(self, section: int, orientation:Qt.Orientation, role: int = ...):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header_data[section] # section == kolona
        elif orientation == Qt.Vertical:
            return super().headerData(section, orientation, role)
